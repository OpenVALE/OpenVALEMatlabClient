%% TODOs




classdef openVALEClient
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        in;
        out;
        sock;
        verbose;
        
        errCodes = {'Command Syntax',...		
        'Command not recognize',...
        'Failed to parse XYZ values',...
        'Failed to parse mask',...
        'Failed to parse boolean',...
        'Parameter out of range',...
        'Speaker number not found',...
        'Failed to parse color value',...
        'Failed to load HRTF',...
        'AudioServer3 error',...
        'Source ID must be an integer',...
        'HRTF ID muse be an integer',...
        'Failed to parse AudioServer3 response',...
        'Failed to initialize location',...
        'Failed to start render'};
        
        
    end
    
    methods (Access = public)
        function obj = openVALEClient(ipStr,portNum,verbose)
            
            if nargin < 3
                obj.verbose = 1;
            else
                obj.verbose = verbose>0;
            end
            
            import java.lang.String;
            import java.io.BufferedReader;
            import java.io.IOException;
            import java.io.InputStream;
            import java.io.InputStreamReader;
            import java.io.OutputStream;
            import java.net.Socket;
            
            try
                obj.sock = Socket(ipStr, portNum);
                if(obj.sock.isConnected())
                    disp(['Connected to openVALEServer at ' ipStr ':' num2str(portNum)]);
                    obj.in = java.io.BufferedReader(java.io.InputStreamReader(getInputStream(obj.sock)));
                    obj.out = java.io.PrintWriter(getOutputStream(obj.sock),true);
                else
                    error([mfilename ': Failed to Connect!']);
                end
            catch
                st = dbstack;
                disp(st);
                error([mfilename ': Socket Error!']);
            end
        end
        
        %% getSubjectNumber()

        function subNum = getSubjectNumber(obj)
            
            cmdStr = 'getSubjectNumber()';
     
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            subNum = obj.processRtnMsg(rtn,'getSubjectNumber');

        end
        
        %% setHRTF(sourceNumber,HRTFid)
		%- Assign an HRTF to the source
		%- Example setHRTF(0,0)

        function setHRTF(obj,hrtfID)
            
            cmdStr = ['setHRTF(' hrtfID ')'];
     
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'setHRTF');

        end
        
        %% hrtfID = loadHRTF(filename)
        %		- Loads an HRTF file
        %		- if successful will return the OpenVALE ID for the HRTF which can be used for future calls
        %		- Example 1: id = loadHRTF(KEMAR.slh)
        function hrtfID = loadHRTF(obj,filename)
            
            cmdStr = ['loadHRTF(' filename ')'];
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            [hrtfID] = obj.processRtnMsg(rtn,'loadHRTF');

        end
        
        
        %% srcID = addAudioSource(Type, initialPosition, HRTFid, extraParams)
        %    - Initializes source in the sound engine and returns the OpenVALE reference for the source
        %    - Current supported values for Type are: wav, noiseGen, ASIO
        %    - InitialPosition is of format [f,l,t]
        %    - extraParams for wav source filename=wavname.wav
        %    - extraParams for asio source number of channels integer
        %    - Example 1: addAudioSource(wav,[2,0,1],0,filename=wavname.wav)
        %    - Example 2: addAudioSource(noiseGen,[3,1,3],0)
        %    - Example 3: addAudioSource(asio,[3,1,3],0,6)

        
        
        function srcID = addAudioSource(obj,type,initialPosition,hrtfID,extraParam)
            if strcmpi(type,'noiseGen')
                    extraParam = '';
            end
            
            cmdStr = ['addAudioSource(' type,',['...
                num2str(initialPosition(1)),',',...
                num2str(initialPosition(2)),','...
                num2str(initialPosition(3)),'],',...
                num2str(hrtfID),',',...
                num2str(extraParam) ')'];
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            [srcID] = obj.processRtnMsg(rtn,'addAudioSource');
        end
        
        
        
        %% enableSrc(srcID,T/F)
        %    - Enable or disable a source based on the boolean passed
        %    - Example enableSrc(1,T)

        function enableSrc(obj,srcID,flag)
            cmdStr = ['enableSrc(' num2str(srcID) ',' flag ')'];
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'enableSrc');
        end
        
        
        %% startRendering()
        %    - Sets the sound engine to render mode
        %    - All unmuted and enabled source begin play
        %    - Example startRendering()

        function startRendering(obj)
            cmdStr = 'startRendering()';
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'startRendering');
        end
        
        %% defineFront() 
        %    - define the origin of position and azimuth based on the HMDs current orientation and position

        function defineFront(obj)
            cmdStr = 'defineFront()';
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'defineFront');
        end
        
        
        %% adjustSourceLevel(srcID,relativeLevel)
        %    - Adjust the relative level of an audio source
        %    - srcID is the OpenVALE audio source ID
        %    - relative level is in dB
        %    - Example adjustSourceLevel(1,5)

        function adjustSourceLevel(obj,srcID,relativeLevel)
            cmdStr = ['adjustSourceLevel(' num2str(srcID) ',' num2str(relativeLevel) ')'];
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'adjustSourceLevel'); 
        end
        
        %% adjustOverallLevel(relativeLevel)
        %    - Adjust the relative level of all sources
        %    - relative level is in dB
        %    - Example adjustOverallLevel(5)

        function adjustOverallLevel(obj, relativeLevel)
            cmdStr = ['adjustOverallLevel(' num2str(relativeLevel) ')'];
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'adjustOverallLevel');
        end
        
        
        %% adjustSourcePosition(srcID, Position = [x,y,z] or SpkID)
        %    - Moves a source to a new position
        %    - srcID is the OpenVALE audio source ID
        %    - Position of the format [f,l,t]
        %    - spkID is the ID of the speaker in ALF
        %    - Example 1: adjustSourcePosition(1,[1,2,3])
        %    - Example 2: adjustSourcePosition(1,274)

        function adjustSourcePosition(obj, srcID, newPosition)
            if numel(newPosition) == 1
                cmdStr = ['adjustSourcePosition(' num2str(srcID) ',' num2str(newPosition) ')'];
            elseif numel(newPosition) == 3
                cmdStr = ['adjustSourcePosition(' num2str(srcID) ',['...
                    num2str(newPosition(1)) ',' ...
                    num2str(newPosition(2)) ',' ...
                    num2str(newPosition(3)) '])'];
            end
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'adjustSourcePosition');
        end
        
        
        %% muteAudioSource(srcID,T/F)
        %    - Mutes an audio source 
        %    - Example muteAudioSource(1,T)

        function muteAudioSource(obj,srcID,flag)
            cmdStr = ['muteAudioSource(' num2str(srcID) ',' flag ')'];
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'muteAudioSource');
        end
        
        
        %% setLEDs(Position = [x,y,z] or SpkID, LEDMask = [1,0,0,1])
        %    - Set LEDs on/off based on nearest speaker to position or speaker ID
        %    - For LEDMask 1 = turn LED on, 0 = turn LED off
        %    - Example setLEDs([f,l,t],[1,0,1,0])
        %    - Example setLEDs(1,[1,0,1,0])

        function setLEDs(obj,Position,LEDMask)
            if numel(Position) == 1
                cmdStr = ['setLEDs(' num2str(Position) ',[' ...
                    num2str(LEDMask(1)) ',' ...
                    num2str(LEDMask(2)) ',' ...
                    num2str(LEDMask(3)) ',' ...
                    num2str(LEDMask(4)) '])'];
            elseif numel(Position) == 3
                cmdStr = ['setLEDs(['...
                    num2str(Position(1)) ',' ...
                    num2str(Position(2)) ',' ...
                    num2str(Position(3)) '], ['...
                    num2str(LEDMask(1)) ',' ...
                    num2str(LEDMask(2)) ',' ...
                    num2str(LEDMask(3)) ',' ...
                    num2str(LEDMask(4)) '])'];
            end
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'setLEDs');
            
        end
        
        %% showFreeCursor(whichCursor = {'head','hand'}, T/F)
        %    - Shows the cursor attached to chosen object
        %    - Example showFreeCursor(head,T)

        function showFreeCursor(obj, whichCursor, flag)
            cmdStr = ['showFreeCursor(' whichCursor ',' flag ')'];
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'showFreeCursor');
        end
        
        %% showSnappedCursor(whichCursor = {'head','hand'}, T/F)
        %    - Turns on the LEDs of the speaker the user is pointing toward
        %    - Example showSnappedCursor(head,T)

        function showSnappedCursor(obj, whichCursor, flag)
            cmdStr = ['showSnappedCursor(' whichCursor ',' flag ')'];
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'showSnappedCursor');
        end
        
        
        %% [spkID, respTime] = waitForResponse()
        %    - Waits for the user to press the trigger on the right hand controller 
        %    - Returns nearest speaker ID to the currently active cursor
        %    - if no cursor is active, returns the nearest speaker to free cursor attached to the head

        function [spkID, respTime] = waitForResponse(obj)
            cmdStr = 'waitForResponse()';
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            [spkID, respTime] = obj.processRtnMsg(rtn,'waitForResponse');
        end
        
        %% waitForReCenter(spkID or position,tolerance)
        %    - Waits for the user to face a specified position or speaker
        %    - Tolerance is in radians
        %    - Example 1 : waitForReCenter(1,.1)
        %    - Example 2 : waitForReCenter([1,2,3], .1)

        function waitForReCenter(obj, Position, tolerance)
            if numel(Position) == 1
                cmdStr = ['waitForReCenter(' num2str(Position) ',' num2str(tolerance) ')'];
            elseif numel(newPosition) == 3
                cmdStr = ['waitForReCenter(['...
                    num2str(Position(1)) ',' ...
                    num2str(Position(2)) ',' ...
                    num2str(Position(3)) '] ,' num2str(tolerance) ')'];
            end
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'waitForReCenter');
            
        end
        
        %% [orientation] = getHeadOrientation()
        %    - returns the heads current orientation in Yaw Pitch Roll Radians

        function [orientation] = getHeadOrientation(obj)
            cmdStr = 'getHeadOrientation()';
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            orientation = obj.processRtnMsg(rtn,'getHeadOrientation');
        end
        
        %% [position,orientation] = getHead6DOF()
        %    - returns the position in FLT meters
        %    - returns orientation in YPR Radians

        function [position,orientation] = getHead6DOF(obj)
            cmdStr = 'getHead6DOF()';
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            [position,orientation] = obj.processRtnMsg(rtn,'getHead6DOF');
        end
        
        %% [spkID,errorDistance] = getNearestSpeaker(position) 
        %    - Returns the nearest speaker to the given position
        %    - error distance in is meters
        %    - Example : getNearestSpeaker([1,2,3])

        function [spkID,errorDistance] = getNearestSpeaker(obj, Position)
            cmdStr = ['getNearestSpeaker([' ...
                num2str(Position(1)) ',' ...
                num2str(Position(2)) ',' ...
                num2str(Position(3)) '])'];
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            [spkID,errorDistance] = obj.processRtnMsg(rtn,'getNearestSpeaker');
        end
        
        
        %% [position] = getSpeakerPosition(spkID) 
        %    - Returns the FLT position of the speaker ID
        %    - Example : getSpeakerPosition(1)

        function position = getSpeakerPosition(obj, spkID)
            cmdStr = ['getSpeakerPosition(' num2str(spkID) ')'];
            
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            position = obj.processRtnMsg(rtn,'getSpeakerPosition');
        end
        
        
        %% highlightlocation(spkID or position,color)
        %    - Shows a highlighted location with a specified color
        %    - Color format [r,g,b] where r g b are between 0,1
        %    - Calling highlightLocation() will hide the highlight
        %    - Example : highlightlocation(274,[1,.5,1])
        %    - Example : highlightlocation([2,1.7,4],[1,.5,1])

        function rtn = highlightLocation(obj,Position,Color)
            if (nargin < 2)
                cmdStr = 'highlightLocation()';
            else
                if numel(Position) == 1
                    cmdStr = ['highlightLocation(' num2str(Position) ',[' ...
                        num2str(Color(1)) ',' ...
                        num2str(Color(2)) ',' ...
                        num2str(Color(3)) '])'];
                elseif numel(Position) == 3
                    cmdStr = ['highlightLocation(['...
                        num2str(Position(1)) ',' ...
                        num2str(Position(2)) ',' ...
                        num2str(Position(3)) '], ['...
                        num2str(Color(1)) ',' ...
                        num2str(Color(2)) ',' ...
                        num2str(Color(3)) '])'];
                end
            end
            obj.sendStrMsg(cmdStr)
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'highlightLocation');
        end
        
        %% reset()
        %   - resets openVALE
        
        function reset(obj)
            obj.sendStrMsg('reset()')
            rtn = obj.waitForInBuffer();
            obj.processRtnMsg(rtn,'reset');
        end
        
    end
    
    
    
    methods (Access = private)
        function sendStrMsg(obj,str)
            obj.clearInputBuffer;
            try
                obj.out.println(str);
                if obj.verbose; disp(['>>' str]); end
                
            catch ME
                rethrow(ME)
            end
        end
        
        function clearInputBuffer(obj)
            while(obj.in.ready)
                obj.in.readLine();
            end
        end
        
        function rtn = waitForInBuffer(obj)
            while(~obj.in.ready)
            end
            rtn = char(readLine(obj.in));
            
            if obj.verbose; disp(['<<' rtn]); end
            
        end
        
        function [varargout] = processRtnMsg(obj,rtnStr,expectedFcnName)

            [fcnName,rem] = strtok(rtnStr,',');
            
            if ~strcmpi(fcnName,expectedFcnName)
                fprintf(2,'Unexpected Return Message!\n');
                fprintf(2,'Expected: %s\n',expectedFcnName);
                fprintf(2,'Returned: %s\n',fcnName);
%                 error(' ')
                beep;
            end
            
            [errCheck,rem] = strtok(rem,',');
            
            if str2double(errCheck)
                error([mfilename ':' obj.errCodes{errCheck}]);
            end

            %get rid of leading comma
            rem = rem(2:end);

            i = 1; 
            while ~isempty(rem)

                if strcmp(rem(1),'[')
                    closeInd = strfind(rem,']');
                    varargout{i} = eval(rem(1:closeInd));
                    rem = rem(closeInd+2:end);
                else
                    [strVal,rem] = strtok(rem,',');
                    varargout{i} = str2double(strVal);
                    rem = rem(2:end);
                end
                i = i+1;
            end
            
        end
    end
   
end


