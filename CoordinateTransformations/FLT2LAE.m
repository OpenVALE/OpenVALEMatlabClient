function LAE = FLT2LAE(FLT)
%FLT2LAE Transform Front-Left-Top to LeftAzimuth-Elevation coordinates.
%
%   LeftAzimuth     = + left angle from the Front axis [-pi:pi]
%   Elevation       = + upward angle from horizontal plane [-pi/2:pi/2]
%   
%   FLT - Front-Left-Top (Cartesian, XYZ)
%   LIC - Lateral-Intraconic (Interaural-Polar)
%   RAE - RightAzimuth-Elevation (Psych Spherical, Vertical-Polar)
%   LAE - LeftAzimuth-Elevation (Matlab Spherical, Vertical-Polar)
%   
%   See also FLT2RAE, FLT2LAE, RAE2FLT.

%   GDRomigh 01/13. 

%Check Input
if size(FLT,2)~=3
    error('FLT2LAE: Input must be Nx3 matrix.');
end

%Magnitude of projection onto horizontal-plane 
r = hypot(FLT(:,1),FLT(:,2));

%LeftAzimuth-Elevation Coordinates
LAE(:,1) = atan2(FLT(:,2),FLT(:,1));
LAE(:,2) = atan2(FLT(:,3),r);
LAE(:,3) = hypot(r,FLT(:,3));
