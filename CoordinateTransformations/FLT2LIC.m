function LIC = FLT2LIC(FLT)
%FLT2LIC Transform Front-Left-Top to Lateral-Intraconic coordinates.
%
%   Lateral     = + right angle from the median plane [-pi/2:pi/2]
%   Intraconic  = + upward angle around cone of confusion [-pi:pi]
%   
%   FLT - Front-Left-Top (Cartesian, XYZ)
%   LIC - Lateral-Intraconic (Interaural-Polar)
%   RAE - RightAzimuth-Elevation (Psych Spherical, Vertical-Polar)
%   LAE - LeftAzimuth-Elevation (Matlab Spherical, Vertical-Polar)
%   
%   See also FLT2RAE, FLT2LAE, LIC2FLT.

%   GDRomigh 01/13. 

%Check Input
if size(FLT,2)~=3
    error('FLT2LIC: Input must be Nx3 matrix.');
end

%Magnitude of projection onto median-plane 
r = hypot(FLT(:,1),FLT(:,3));

%Lateral-Intraconic Coordinates
LIC(:,1) = atan2(-FLT(:,2),r);
LIC(:,2) = atan2(FLT(:,3),FLT(:,1));
LIC(:,3) = hypot(r,FLT(:,2));
