function FLT = LIC2FLT(LIC)
%LIC2FLT Transform Lateral-Intraconic to Front-Left-Top coordinates.
%
%   Lateral     = + right angle from the median plane [-pi/2:pi/2]
%   Intraconic  = + upward angle around cone of confusion [-pi:pi]
%   
%   FLT - Front-Left-Top (Cartesian, XYZ)
%   LIC - Lateral-Intraconic (Interaural-Polar)
%   RAE - RightAzimuth-Elevation (Psych Spherical, Vertical-Polar)
%   LAE - LeftAzimuth-Elevation (Matlab Spherical, Vertical-Polar)
%   
%   See also FLT2RAE, FLT2LAE, LIC2FLT.

%   GDRomigh 01/13. 


%Check for proper input
if size(LIC,2)==2 %No range assume 1s
    LIC(:,3) = ones(size(LIC,1),1);
elseif size(LIC,2)~=3
    error('LIC2FLT: Input matrix must be Nx3 (or Nx2 for unit radius).');
end

%Check for degrees instead of radians
if (max(max(abs(LIC(:,1:2)))) > 2*pi)
    warning(['LIC2FLT: Input contains angle values > 2*pi,'...
        'input might be in degrees rather than radians.']);
end

%Get FLT coordinates
FLT(:,1) = LIC(:,3) .* cos(LIC(:,1)) .* cos(LIC(:,2));
FLT(:,2) = LIC(:,3) .* sin(-LIC(:,1));
FLT(:,3) = LIC(:,3) .* cos(LIC(:,1)) .* sin(LIC(:,2));