function FLT = RAE2FLT(RAE)
%RAE2FLT Transform RightAzimuth-Elevation to Front-Left-Top coordinates.
%
%   RightAzimuth    = + right angle from the Front axis [-pi:pi]
%   Elevation       = + upward angle from horizontal plane [-pi/2:pi/2]
%   
%   FLT - Front-Left-Top (Cartesian, XYZ)
%   LIC - Lateral-Intraconic (Interaural-Polar)
%   RAE - RightAzimuth-Elevation (Psych Spherical, Vertical-Polar)
%   LAE - LeftAzimuth-Elevation (Matlab Spherical, Vertical-Polar)
%   
%   See also FLT2RAE, FLT2LAE, RAE2FLT.

%   GDRomigh 01/13. 


%Check for proper input
if size(RAE,2)==2 %No range assume 1s
    RAE(:,3) = ones(size(RAE,1),1);
elseif size(RAE,2)~=3
    error('RAE2FLT: Input matrix must be Nx3 (or Nx2 for unit radius).');
end

%Check for degrees instead of radians
if (max(max(abs(RAE(:,1:2)))) > 2*pi)
    warning(['RAE2FLT: Input contains angle values > 2*pi,'...
        'input might be in degrees rather than radians.']);
end

%Get FLT coordinates
FLT(:,1) = RAE(:,3) .* cos(RAE(:,2)) .* cos(RAE(:,1));
FLT(:,2) = RAE(:,3) .* cos(RAE(:,2)) .* -sin(RAE(:,1));
FLT(:,3) = RAE(:,3) .* sin(RAE(:,2));
