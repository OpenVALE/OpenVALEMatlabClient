function FLT = LAE2FLT(LAE)
%LAE2FLT Transform LeftAzimuth-Elevation to Front-Left-Top coordinates.
%
%   LeftAzimuth    = + left angle from the Front axis [-pi:pi]
%   Elevation       = + upward angle from horizontal plane [-pi/2:pi/2]
%   
%   FLT - Front-Left-Top (Cartesian, XYZ)
%   LIC - Lateral-Intraconic (Interaural-Polar)
%   RAE - RightAzimuth-Elevation (Psych Spherical, Vertical-Polar)
%   LAE - LeftAzimuth-Elevation (Matlab Spherical, Vertical-Polar)
%   
%   See also FLT2RAE, FLT2LAE, RAE2FLT.

%   GDRomigh 01/13. 


%Check for proper input
if size(LAE,2)==2 %No range assume 1s
    LAE(:,3) = ones(size(LAE,1),1);
elseif size(LAE,2)~=3
    error('LAE2FLT: Input matrix must be Nx3 (or Nx2 for unit radius).');
end

%Check for degrees instead of radians
if (max(max(abs(LAE(:,1:2)))) > 2*pi)
    warning(['LAE2FLT: Input contains angle values > 2*pi,'...
        'input might be in degrees rather than radians.']);
end

%Get FLT coordinates
FLT(:,1) = LAE(:,3) .* cos(LAE(:,2)) .* cos(LAE(:,1));
FLT(:,2) = LAE(:,3) .* cos(LAE(:,2)) .* sin(LAE(:,1));
FLT(:,3) = LAE(:,3) .* sin(LAE(:,2));
