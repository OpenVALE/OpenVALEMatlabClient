clear all

addpath('.\CoordinateTransformations\')


% Load spkrtab for angular error calculations
load('ALFGrid.mat','ALF_FLT');
ALF_LAE = FLT2LAE(ALF_FLT);
validSpks = find(ALF_FLT(:,3) > sind(-45));

ovc = openVALEClient('10.0.0.127',43201,1); %% Local Host

ovc.reset();



SLHName = 'GoldenClusterMean_SH6E100_HD280.slh';

hrtfID = ovc.loadHRTF(SLHName);
srcID = ovc.addAudioSource('noiseGen',[2.33,0,0],0);
ovc.startRendering();
ovc.enableSrc(srcID,'T');
ovc.muteAudioSource(srcID,'T');

ovc.waitForResponse();
ovc.defineFront();

%Get SubID does not work yet
% ovc.showFreeCursor('hand','T');
% ovc.getSubjectNumber();
% ovc.showFreeCursor('hand','F');


for i = 1:10
   %% Force Re-center on every trial
    %Turn on Head-slaved Cursor
    ovc.showSnappedCursor('head','T');

    %Turn on highlight at center
    ovc.highlightLocation(274,[0 1 0]);
    
    
    ovc.waitForReCenter(274,5*pi/180);
    
    %Turn off front highlight
    ovc.highlightLocation();

    %Turn off head-slaved cursor
    ovc.showSnappedCursor('head','F');
    %% Position Source at target Location
    
    targetLoc = randi(size(validSpks,1),1,1);
%     ovs.adjustSourcePosition(0,targetLoc);

    targFLT = [ALF_FLT(targetLoc,1)*2.33,ALF_FLT(targetLoc,2)*2.33,ALF_FLT(targetLoc,3)*2.33];
    ovc.adjustSourcePosition(srcID,targFLT);


    %% Capture Head position previous to presentation
    [headFLT,headYPR] = ovc.getHead6DOF();

    
    %% Play Sound (Un-Mute Source)
    st = clock;
    ovc.muteAudioSource(srcID,'F');
    
    burstDur = .25;
    while(etime(clock,st)<burstDur)
        pause(.01);
    end
    ovc.muteAudioSource(srcID,'T');
    
    %% Collect Response
    ovc.showSnappedCursor('hand','T');
    
    [respSpkID,respTime] = ovc.waitForResponse();
    
    %Turn on white highlight at response
    ovc.highlightLocation(respSpkID,[1 1 1]);
    ovc.showSnappedCursor('hand','F');
    
    ovc.muteAudioSource(0,'T'); %In case its continuous
    pause(.5)
    
    %% Give Feedback
    if targetLoc == respSpkID %Correct    
        %Turn on green highlight at target
        ovc.highlightLocation(targetLoc,[0 1 0]);
    else
        %Turn on red highlight at target
        ovc.highlightLocation(targetLoc,[1 0 0]);
    end

    %Wait for them to aknowledge feedback
    ovc.waitForResponse();

    %Turn off target highlight
    ovc.highlightLocation();
    
end
    